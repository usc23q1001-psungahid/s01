class Camper():
    #Properties
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
 
    
    #methods
    def career_track(self):
        print(f'Currently enrolled in {self.course_type} program')
    def info(self):
        print(f'My name is {self.name} of batch {self.batch}')


zuitt_camper = Camper('Patrick', 2009, 'Java Short course')


print(f'Camper Name: {zuitt_camper.name}')
print(f'Camper Batch: {zuitt_camper.batch}')
print(f'Camper Course: {zuitt_camper.course_type}')

zuitt_camper.info()
zuitt_camper.career_track()